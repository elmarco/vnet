use std::env;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

use dbus_codegen::{generate, GenOpts};

fn main() -> Result<(), Box<dyn Error>> {
    let out_dir = env::var("OUT_DIR")?;

    let mut vnet1 = String::new();
    File::open("dbus/vnet1.xml")?.read_to_string(&mut vnet1)?;

    let server = generate(
        &vnet1,
        &GenOpts {
            skipprefix: Some("org.freedesktop.".into()),
            ..Default::default()
        },
    )?;
    let server_path = Path::new(&out_dir).join("dbus-server.rs");
    File::create(server_path)?.write_all(server.as_bytes())?;

    let client = generate(
        &vnet1,
        &GenOpts {
            skipprefix: Some("org.freedesktop.".into()),
            methodtype: None,
            ..Default::default()
        },
    )?;
    let client_path = Path::new(&out_dir).join("dbus-client.rs");
    File::create(client_path)?.write_all(client.as_bytes())?;

    Ok(())
}
