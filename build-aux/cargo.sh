#!/bin/sh

MESON_BUILD_TYPE="$1"

OUTDIR=debug

if [[ "$MESON_BUILD_TYPE" = release ]]
then
    EXTRA_ARGS="--release"
    OUTDIR=release
fi

cargo build --manifest-path "$MESON_SOURCE_ROOT"/Cargo.toml --target-dir="$MESON_BUILD_ROOT" $EXTRA_ARGS
find "$MESON_BUILD_ROOT/$OUTDIR" -maxdepth 1 -type f -executable -exec cp '{}' "$MESON_BUILD_ROOT" \;
