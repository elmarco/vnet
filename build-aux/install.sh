#!/bin/sh

chmod 4755 "${DESTDIR}/${MESON_INSTALL_PREFIX}/libexec/vnet-helper"

ln -sf "${DESTDIR}/${MESON_INSTALL_PREFIX}/bin/vnet" "${DESTDIR}/${MESON_INSTALL_PREFIX}/libexec/qemu-bridge-helper"
