#[macro_use]
extern crate log;
use dbus::arg;
use dbus::blocking::LocalConnection;
use dbus::tree;
use dbus::tree::{Interface, MTFn, MethodErr};
use nix::sched::{unshare, CloneFlags};
use nix::sys::socket::{socketpair, AddressFamily, SockAddr, SockFlag, SockType};
use nix::sys::wait::{waitpid, WaitStatus};
use nix::unistd::{fork, read, write, ForkResult};
use std::error::Error;
use std::fs::DirBuilder;
use std::net::IpAddr;
use std::os::unix::io::{FromRawFd, IntoRawFd};
use std::process::{self, Command};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

mod dbusgen;
mod retain_mut;
use crate::retain_mut::RetainMut;
mod tap;
mod utils;

#[derive(Debug)]
struct Vnetd {}

impl dbusgen::Vnet1 for Vnetd {
    fn exec(&self, cmd: Vec<&str>) -> Result<(Vec<u8>, Vec<u8>, i32), MethodErr> {
        let s = Command::new(cmd[0])
            .args(&cmd[1..])
            .output()
            .map_err(|e| MethodErr::failed(&e))?;

        info!("Exec {:?}", &cmd);
        dbg!("Info {:?}", info());

        let status = s.status.code().unwrap_or(-1);
        Ok((s.stdout, s.stderr, status))
    }

    fn add_tap(
        &self,
        options: Vec<(&str, arg::Variant<Box<dyn arg::RefArg>>)>,
    ) -> Result<(Vec<arg::OwnedFd>, String), tree::MethodErr> {
        let mut tapopt: tap::Options = Default::default();
        for (o, v) in options.iter() {
            match *o {
                "vnet_hdr" => tapopt.vnet_hdr = (v.0).as_u64().unwrap_or(0) != 0,
                _ => eprintln!("option {} not supported", o),
            }
        }
        let (fd, name) = tap::open("ve-tap%d", &tapopt).map_err(|e| MethodErr::failed(&e))?;
        let fd = unsafe { arg::OwnedFd::from_raw_fd(fd.into_raw_fd()) };
        Ok((vec![fd], name))
    }

    fn add_veth(
        &self,
        pid: u32,
        options: Vec<(&str, arg::Variant<Box<dyn arg::RefArg>>)>,
    ) -> Result<String, tree::MethodErr> {
        let name = format!("ve-p{}", pid);

        for (o, _v) in options.iter() {
            match *o {
                _ => eprintln!("option {} not supported", o),
            }
        }

        let success = Command::new("ip")
            .args(&[
                "link",
                "add",
                &name,
                "type",
                "veth",
                "peer",
                "name",
                "host0",
                "netns",
                &pid.to_string(),
            ])
            .status()
            .map_err(|e| MethodErr::failed(&e))?
            .success();
        if !success {
            return Err(MethodErr::failed("ip link add failed!"));
        }

        Ok(name)
    }
}

fn info() -> Vec<IpAddr> {
    let addrs = nix::ifaddrs::getifaddrs().unwrap();
    let mut res = vec![];
    for ifaddr in addrs {
        if ifaddr.interface_name != "host0" {
            continue;
        }
        if let Some(SockAddr::Inet(inet)) = ifaddr.address {
            res.push(inet.ip().to_std());
        }
        if let Some(SockAddr::Inet(netmask)) = ifaddr.netmask {
            res.push(netmask.ip().to_std());
        }
    }
    res
}

#[derive(Copy, Clone, Default, Debug)]
struct TData;
impl dbus::tree::DataType for TData {
    type Tree = ();
    type ObjectPath = Arc<Vnetd>;
    type Property = ();
    type Interface = ();
    type Method = ();
    type Signal = ();
}

fn create_iface() -> Interface<MTFn<TData>, TData> {
    let f = tree::Factory::new_fn();
    dbusgen::vnet1_server(&f, (), |m| {
        let a: &Arc<Vnetd> = m.path.get_data();
        let b: &Vnetd = &a;
        b
    })
}

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::Builder::from_env("LOG").init();
    let runtime_dir = std::path::Path::new(&std::env::var("XDG_RUNTIME_DIR")?).join("vnetd");
    DirBuilder::new().recursive(true).create(&runtime_dir)?;

    let v = Arc::new(Vnetd {});
    let quit = Arc::new(AtomicBool::new(false));

    let mut c = LocalConnection::new_session()?;
    let iface = Arc::new(create_iface());
    c.request_name("org.freedesktop.vnet1", false, false, true)?;
    let f = dbus::tree::Factory::new_fn();
    let tree = f
        .tree(())
        .add(
            f.object_path("/org/freedesktop/vnet1", v.clone())
                .introspectable()
                .add(iface.clone()),
        )
        .add(f.object_path("/", v.clone()).introspectable());
    tree.start_receive(&c);
    info!("Starting");

    let pid = process::id();
    let uid = nix::unistd::Uid::current();
    let gid = nix::unistd::Gid::current();

    let mut bg_commands = vec![];
    let mut buf = vec![1];

    let (pp, pc) = socketpair(
        AddressFamily::Unix,
        SockType::Stream,
        None,
        SockFlag::empty(),
    )?;

    match fork()? {
        ForkResult::Child => {
            info!("wait for parent");
            assert_eq!(read(pc, &mut buf)?, 1);

            let user = nix::unistd::User::from_uid(uid).unwrap().unwrap();
            let group = nix::unistd::Group::from_gid(gid).unwrap().unwrap();

            let map = utils::get_submap(&user.name, "/etc/subuid")?;
            // we need extra mappings for systemd-network.. FIXME
            let c = Command::new("newuidmap")
                .args(&[
                    &nix::unistd::Pid::parent().to_string(),
                    "0",
                    &uid.to_string(),
                    "1",
                    "1",
                    &map.0,
                    &map.1,
                ])
                .status()?;
            info!("newuidmap success: {}", c.success());

            let map = utils::get_submap(&group.name, "/etc/subgid")?;
            let c = Command::new("newgidmap")
                .args(&[
                    &nix::unistd::Pid::parent().to_string(),
                    "0",
                    &gid.to_string(),
                    "1",
                    "1",
                    &map.0,
                    &map.1,
                ])
                .status()?;
            info!("newgidmap success: {}", c.success());

            info!("wait for parent");
            write(pc, &[1]).unwrap_or_else(|e| panic!("write to child: {}", e));
            assert_eq!(read(pc, &mut buf)?, 1);

            info!("run helper");
            let c = Command::new("/usr/libexec/vnet-helper")
                .arg("--force")
                .arg(pid.to_string())
                .status()?;
            info!("helper success: {}", c.success());

            return Ok(());
        }
        ForkResult::Parent { child, .. } => {
            unshare(CloneFlags::CLONE_NEWNS | CloneFlags::CLONE_NEWUSER)
                .unwrap_or_else(|e| panic!("unshare(newuser): {}", e));
            unshare(CloneFlags::CLONE_NEWNET).unwrap_or_else(|e| panic!("unshare(newnet): {}", e));

            let quit_clone = quit.clone();
            ctrlc::set_handler(move || {
                println!("received Ctrl+C!");
                quit_clone.store(true, Ordering::SeqCst);
            })
            .unwrap_or_else(|e| info!("Did not set ctrl-c handler: {}", e));

            write(pp, &[1]).unwrap_or_else(|e| panic!("write to child: {}", e));
            assert_eq!(read(pp, &mut buf)?, 1);

            info!("setuid root");
            nix::unistd::setuid(nix::unistd::ROOT)?;

            info!("run: mount new /run/dbus");
            let success = Command::new("mount")
                .args(&["-t", "tmpfs", "-o", "size=512M", "tmpfs", "/run/dbus"])
                .status()?
                .success();
            assert!(success);

            info!("run: mount new /run/udev");
            let success = Command::new("mount")
                .args(&["-t", "tmpfs", "-o", "size=512M", "tmpfs", "/run/udev"])
                .status()?
                .success();
            assert!(success);

            info!("run: mount new /run/systemd");
            let success = Command::new("mount")
                .args(&["-t", "tmpfs", "-o", "size=512M", "tmpfs", "/run/systemd"])
                .status()?
                .success();
            assert!(success);

            info!("run: mount new /sys");
            let success = Command::new("mount")
                .args(&["-t", "sysfs", "sysfs", "/sys"])
                .status()?
                .success();
            assert!(success);

            info!("run: bind mount /run/xtables.lock");
            let xtables = runtime_dir.join("xtables.lock");
            utils::touch(&xtables)?;
            let success = Command::new("mount")
                .args(&["--bind", xtables.to_str().unwrap(), "/run/xtables.lock"])
                .status()?
                .success();
            assert!(success);

            info!("run: bind mount /usr/lib/systemd/network");
            let success = Command::new("mount")
                .args(&[
                    "--bind",
                    "/usr/lib/vnet/systemd/network",
                    "/usr/lib/systemd/network",
                ])
                .status()?
                .success();
            assert!(success);

            info!("run: dbus");
            let c = Command::new("dbus-daemon")
                .arg("--system")
                .arg("--nofork")
                .spawn()?;
            bg_commands.push(("dbus-daemon", c));

            info!("run: systemd-udevd");
            let c = Command::new("/usr/lib/systemd/systemd-udevd").spawn()?;
            bg_commands.push(("systemd-udevd", c));

            info!("run: systemd-networkd");
            let c = Command::new("/usr/lib/systemd/systemd-networkd").spawn()?;
            bg_commands.push(("systemd-networkd", c));

            // FIXME: there seems to be some intiialization race.. :(
            std::thread::sleep(std::time::Duration::new(1, 0));

            write(pp, &[1]).unwrap_or_else(|e| panic!("write to child: {}", e));
            info!("wait for child");
            assert_eq!(waitpid(child, None), Ok(WaitStatus::Exited(child, 0)));

            info!("run: ip link set up lo host0");
            for iface in &["lo", "host0"] {
                let success = Command::new("ip")
                    .args(&[
                        "link",
                        "set",
                        "up",
                        iface,
                    ])
                    .status()?
                .success();
                assert!(success);
            }
        }
    };

    while !quit.load(Ordering::SeqCst) {
        debug!("loop");

        bg_commands.retain_mut(|c| {
            if let Ok(Some(s)) = c.1.try_wait() {
                println!("{} exited with status: {:?}", c.0, s);
                false
            } else {
                true
            }
        });

        c.process(std::time::Duration::new(1, 0))?;
    }

    for c in bg_commands.iter_mut() {
        c.1.kill()
            .unwrap_or_else(|e| eprintln!("failed to kill {}: {}", c.0, e));
        if let Ok(s) = c.1.wait() {
            println!("{} exited with status: {:?}", c.0, s);
        }
    }

    info!("bye");
    Ok(())
}
