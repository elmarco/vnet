use std::fs::File;
use std::fs::OpenOptions;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::{Error, ErrorKind};
use std::path::Path;

pub fn touch(path: &Path) -> io::Result<()> {
    match OpenOptions::new().create(true).write(true).open(path) {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    }
}

pub fn get_submap<P: AsRef<Path>>(name: &str, path: P) -> io::Result<(String, String)> {
    // this only returns the first map, should be ok..
    for l in BufReader::new(File::open(path)?)
        .lines()
        .map(|l| l.unwrap())
    {
        let e: Vec<_> = l.split(':').collect();

        if e.len() == 3 && e[0] == name {
            return Ok((e[1].to_string(), e[2].to_string()));
        }
    }

    Err(Error::new(
        ErrorKind::Other,
        "couldn't find submap uid/gid entry",
    ))
}
