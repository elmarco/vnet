use netlink_packet_route::NetlinkPayload::InnerMessage;
use netlink_packet_route::RtnlMessage::NewLink;
use std::error::Error;
use std::io;

use netlink_packet_core::{AckMessage, NetlinkHeader, NetlinkMessage, NetlinkPayload};
use netlink_packet_route::constants::*;
use netlink_packet_route::link::nlas::InfoKind;
use netlink_packet_route::nlas::link::{Info, InfoData, Nla, VethInfo};
use netlink_packet_route::{LinkMessage, RtnlMessage};
use netlink_sys::Socket;

pub fn get_ifindex(socket: &Socket, ifname: &str) -> Result<Option<u32>, Box<dyn Error>> {
    let mut packet = NetlinkMessage {
        header: NetlinkHeader::default(),
        payload: NetlinkPayload::from(RtnlMessage::GetLink(LinkMessage::default())),
    };
    packet.header.flags = NLM_F_DUMP | NLM_F_REQUEST;
    packet.header.sequence_number = 1;
    packet.finalize();

    let mut buf = vec![0; packet.header.length as usize];
    assert!(buf.len() == packet.buffer_len());
    packet.serialize(&mut buf[..]);

    socket.send(&buf[..], 0)?;

    let mut receive_buffer = vec![0; 4096];
    let mut offset = 0;
    let mut found = None;

    loop {
        let size = socket.recv(&mut receive_buffer[..], 0)?;

        loop {
            let bytes = &receive_buffer[offset..];
            let rx_packet: NetlinkMessage<RtnlMessage> =
                NetlinkMessage::deserialize(bytes).unwrap();

            match rx_packet.payload {
                InnerMessage(NewLink(LinkMessage { header, nlas })) => {
                    for nla in nlas {
                        match nla {
                            Nla::IfName(name) if name == ifname => found = Some(header.index),
                            _ => continue,
                        }
                    }
                }
                NetlinkPayload::Done => {
                    return Ok(found);
                }
                _ => continue,
            }

            offset += rx_packet.header.length as usize;
            if offset == size || rx_packet.header.length == 0 {
                offset = 0;
                break;
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Default)]
pub struct LinkDelRequest {
    message: LinkMessage,
}

impl LinkDelRequest {
    pub(crate) fn new(index: u32) -> Self {
        let mut message = LinkMessage::default();
        message.header.index = index;
        LinkDelRequest { message }
    }

    pub fn exec(self, socket: &Socket) -> Result<(), Box<dyn Error>> {
        let mut buf = vec![0; 1024 * 8];

        let mut nl = self.to_nl();
        nl.finalize();
        nl.serialize(&mut buf[..]);
        socket.send(&buf[..], 0)?;
        let size = socket.recv(&mut buf[..], 0)?;
        let bytes = &buf[..size];
        let rx_packet: NetlinkMessage<RtnlMessage> = NetlinkMessage::deserialize(bytes).unwrap();
        if NetlinkPayload::Done == rx_packet.payload {
            return Ok(());
        }

        Err(io::Error::new(io::ErrorKind::NotFound, "interface not found").into())
    }

    fn to_nl(self) -> NetlinkMessage<RtnlMessage> {
        self.into()
    }
}

impl From<LinkDelRequest> for NetlinkPayload<RtnlMessage> {
    fn from(la: LinkDelRequest) -> Self {
        RtnlMessage::DelLink(la.message).into()
    }
}

impl From<LinkDelRequest> for NetlinkMessage<RtnlMessage> {
    fn from(la: LinkDelRequest) -> Self {
        NetlinkMessage {
            header: NetlinkHeader {
                flags: NLM_F_REQUEST | NLM_F_ACK,
                ..Default::default()
            },
            payload: la.into(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Default)]
pub struct LinkAddRequest {
    message: LinkMessage,
}

impl LinkAddRequest {
    fn new() -> Self {
        LinkAddRequest {
            message: LinkMessage::default(),
        }
    }

    pub fn veth(name: &str, peer_name: &str, nspid: Option<u32>) -> Self {
        let mut peer = LinkMessage::default();
        peer.nlas.push(Nla::IfName(peer_name.to_string()));
        if let Some(nspid) = nspid {
            peer.nlas.push(Nla::NetNsPid(nspid));
        }
        let link_info_data = InfoData::Veth(VethInfo::Peer(peer));
        Self::new()
            .name(name)
            .up() // iproute2 does not set this one up
            .link_info(InfoKind::Veth, Some(link_info_data))
    }

    fn up(mut self) -> Self {
        self.message.header.flags = IFF_UP;
        self.message.header.change_mask = IFF_UP;
        self
    }

    fn link_info(self, kind: InfoKind, data: Option<InfoData>) -> Self {
        let mut link_info_nlas = vec![Info::Kind(kind)];
        if let Some(data) = data {
            link_info_nlas.push(Info::Data(data));
        }
        self.append_nla(Nla::Info(link_info_nlas))
    }

    fn name(mut self, name: &str) -> Self {
        self.message.nlas.push(Nla::IfName(name.to_string()));
        self
    }

    fn append_nla(mut self, nla: Nla) -> Self {
        self.message.nlas.push(nla);
        self
    }

    pub fn to_nl(self) -> NetlinkMessage<RtnlMessage> {
        self.into()
    }

    pub fn exec(self, socket: &Socket) -> Result<(), Box<dyn Error>> {
        let mut buf = vec![0; 1024 * 8];

        let mut nl = self.to_nl();
        nl.finalize();
        nl.serialize(&mut buf[..]);
        socket.send(&buf[..], 0)?;

        let size = socket.recv(&mut buf[..], 0)?;
        let bytes = &buf[..size];
        let rx_packet: NetlinkMessage<RtnlMessage> = NetlinkMessage::deserialize(bytes).unwrap();

        if let NetlinkPayload::Ack(AckMessage { code: 0, header: _ }) = rx_packet.payload {
            return Ok(());
        }
        Err(io::Error::new(io::ErrorKind::Other, "failed to add interface").into())
    }
}

impl From<LinkAddRequest> for NetlinkPayload<RtnlMessage> {
    fn from(la: LinkAddRequest) -> Self {
        RtnlMessage::NewLink(la.message).into()
    }
}

impl From<LinkAddRequest> for NetlinkMessage<RtnlMessage> {
    fn from(la: LinkAddRequest) -> Self {
        NetlinkMessage {
            header: NetlinkHeader {
                flags: NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL | NLM_F_ACK,
                ..Default::default()
            },
            payload: la.into(),
        }
    }
}
