use netlink_sys::{Protocol, Socket, SocketAddr};
use std::env;
use std::error::Error;
use std::fs::metadata;
use std::os::unix::fs::MetadataExt;
use std::path::Path;

mod netlink;

fn main() -> Result<(), Box<dyn Error>> {
    unsafe {
        libc::clearenv();
    }

    let args: Vec<String> = env::args().collect();

    let (force, nspid) = match args.len() {
        2 => (false, args[1].parse::<u32>()?),
        3 => {
            assert_eq!(args[1], "--force");
            (true, args[2].parse::<u32>()?)
        }
        _ => panic!(),
    };

    let uid = unsafe { libc::getuid() };
    let procpid = Path::new("/proc").join(nspid.to_string());
    let meta = metadata(&procpid)?;

    if uid != 0 && meta.uid() != uid {
        panic!("{} PID doesn't belong to UID {}", nspid, uid);
    }

    /* FIXME: policykit check */
    unsafe { libc::setreuid(0, 0) };

    let ifname = format!("ve-{}", uid);
    let mut socket = Socket::new(Protocol::Route)?;
    let _port_number = socket.bind_auto()?.port_number();
    socket.connect(&SocketAddr::new(0, 0))?;

    if force {
        let ifindex = netlink::get_ifindex(&socket, &ifname)?;
        if let Some(ifindex) = ifindex {
            let _ = netlink::LinkDelRequest::new(ifindex).exec(&socket);
        }
    }

    netlink::LinkAddRequest::veth(&ifname, "host0", Some(nspid)).exec(&socket)
}
