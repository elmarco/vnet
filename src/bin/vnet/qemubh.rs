use structopt::StructOpt;

use crate::{AddCommand, Cli, Command};

/// qemu-bridge-helper compatibility
#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
struct QemuBHCli {
    #[structopt(short, long)]
    debug: bool,

    #[structopt(long)]
    use_vnet: bool,

    #[structopt(long)]
    br: Option<String>,

    #[structopt(long)]
    fd: i32,
}

pub(crate) fn from_args() -> Cli {
    let args = QemuBHCli::from_args();

    Cli {
        debug: args.debug,
        cmd: Command::Add(AddCommand::Tap {
            fd: args.fd,
            vnet_hdr: args.use_vnet,
            bridge: args.br,
        }),
    }
}
