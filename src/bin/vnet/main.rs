use dbus::arg::RefArg;
use dbus::arg::Variant;
use dbus::blocking::Connection;
use nix::sys::socket::sendmsg;
use nix::sys::socket::ControlMessage;
use nix::sys::socket::MsgFlags;
use nix::sys::uio::IoVec;
use quicli::prelude::*;
use std::io::{self, Write};
use std::os::unix::io::RawFd;
use std::os::unix::process::CommandExt;
use std::time::Duration;
use structopt::StructOpt;

mod dbusgen;
use dbusgen::Vnet1;
mod qemubh;

#[derive(Debug)]
struct Vnet<'a, C> {
    proxy: dbus::blocking::Proxy<'a, C>,
}

/// Vnet
#[derive(Debug, StructOpt)]
struct Cli {
    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,

    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
enum Command {
    Exec {
        #[structopt()]
        args: Vec<String>,
    },
    Enter,
    Add(AddCommand),
    Info,
}

#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
enum AddCommand {
    Tap {
        #[structopt(long)]
        fd: i32,

        #[structopt(long)]
        vnet_hdr: bool,

        #[structopt(long)]
        bridge: Option<String>,
    },
    Veth {
        #[structopt(long)]
        pid: u32,
    },
}

fn command_enter() -> CliResult {
    let c = Connection::new_session()?;

    let p = c.with_proxy(
        "org.freedesktop.DBus",
        "/org/freedesktop/DBus",
        Duration::from_millis(5000),
    );
    let (uid,): (u32,) = p.method_call(
        "org.freedesktop.DBus",
        "GetConnectionUnixProcessID",
        ("org.freedesktop.vnet1",),
    )?;
    /* setns() needs sysadmin... */
    /* FIXME: other options would involve pty master */
    std::process::Command::new("pkexec")
        .args(&["nsenter", "-a", "-t", &uid.to_string()])
        .exec();

    unreachable!();
}

fn command_add<'a, C>(v: &Vnet<'a, C>, cmd: AddCommand) -> CliResult
where
    dbus::blocking::Proxy<'a, C>: Vnet1,
{
    match cmd {
        AddCommand::Tap {
            fd,
            vnet_hdr,
            bridge,
        } => {
            let mut options = vec![];
            if vnet_hdr {
                let x = Box::new(true) as Box<dyn RefArg>;
                options.push(("vnet_hdr", Variant(x)));
            }
            if let Some(br) = bridge {
                let x = Box::new(br) as Box<dyn RefArg>;
                options.push(("bridge", Variant(x)));
            }
            let (fds, id) = v.proxy.add_tap(options)?;
            let fds: Vec<i32> = fds.into_iter().map(|fd| fd.into_fd()).collect();
            let iov = [IoVec::from_slice(b"\0")];
            let cmsg = ControlMessage::ScmRights(&fds);

            println!("ID={}", id);
            let r = sendmsg(fd as RawFd, &iov, &[cmsg], MsgFlags::empty(), None)?;
            assert_eq!(r, 1);
        }
        AddCommand::Veth { pid } => {
            let options = vec![];
            let id = v.proxy.add_veth(pid, options)?;
            println!("ID={}", id);
        }
    }

    Ok(())
}

fn command_exec<'a, C>(v: &Vnet<'a, C>, args: Vec<String>) -> CliResult
where
    dbus::blocking::Proxy<'a, C>: Vnet1,
{
    let args: Vec<&str> = args.iter().map(AsRef::as_ref).collect();
    let (out, err, code) = v.proxy.exec(args)?;
    io::stdout().write_all(&out)?;
    io::stderr().write_all(&err)?;
    std::process::exit(code)
}

fn main() -> CliResult {
    let prg = std::path::PathBuf::from(std::env::args().nth(0).unwrap());
    let args = if prg.file_name().unwrap() == "qemu-bridge-helper" {
        qemubh::from_args()
    } else {
        Cli::from_args()
    };

    if args.debug {
        std::env::set_var("LOG", "debug");
    }
    env_logger::Builder::from_env("LOG").init();

    let c = Connection::new_session()?;
    let proxy = c.with_proxy(
        "org.freedesktop.vnet1",
        "/org/freedesktop/vnet1",
        Duration::from_millis(5000),
    );

    let v = Vnet { proxy };

    info!("arg: {:?}", args);
    match args.cmd {
        Command::Enter => command_enter()?,
        Command::Exec { args } => command_exec(&v, args)?,
        Command::Add(cmd) => command_add(&v, cmd)?,
        Command::Info => println!("Net: {}", 1),
    }
    Ok(())
}
